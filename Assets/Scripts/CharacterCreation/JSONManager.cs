﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//wrapper for json and file 
public static class JSONManager
{
    public static void CreateDirectory(string filePath)
    {
        if (!Directory.Exists(filePath))
            Directory.CreateDirectory(filePath);
    }

    public static T LoadFile<T>(string fileDirectory)
    {
        if (!File.Exists(fileDirectory))
            return default(T);

        var jsonString = File.ReadAllText(fileDirectory);

        return JsonUtility.FromJson<T>(jsonString);
    }

    public static void SaveFile<T>(string fileDirectory, T data)
    {
        //create or open file
        var saveFile = File.Open(fileDirectory, FileMode.OpenOrCreate);
        saveFile.Close();

        var jsonString = JsonUtility.ToJson(data, true);
        File.WriteAllText(fileDirectory, jsonString);
    }

}
