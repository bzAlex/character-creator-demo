﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CharacterCreator : MonoBehaviour
{
    CharacterData data;
    Character character;
    public Character Character { get { return character; } }

    public Transform characterPos;

    public List<Character> CharacterDatabase;    //List of available Class
    public List<String> NamesDatabase;

    [Header("UI")]
    public InputField NameInput;
    public Text ClassDescription;
    public GameObject Panel;

    //Events
    public event Action OnCharacterCreate;
    public event Action<CharacterData> OnSaveCharacter; 

    private void Start()
    {
        Panel.SetActive(false);
    }

    public void CreateNewData()
    {
        //dispose previous character
        DeleteDisplayedCharacter();

        Panel.SetActive(true);
        data = new CharacterData();
        OnCharacterCreate.Invoke();
    }

    public void LoadData(CharacterData data)
    {
        Panel.SetActive(true);
        this.data = data;
        CreateCharacter(data.Job);
        character.CharacterName = data.characterName;
        NameInput.text = character.CharacterName;

        //Stats
        character.GetComponent<Stats>().attributes = data.attributesData;
        //Features
        character.GetComponent<Featureable>().LoadFeatures(data.featuresData);
        //Skills
        character.GetComponent<Skillable>().LoadSkills(data.skillsIndex);
        OnCharacterCreate.Invoke();
    }

    public void SaveData()
    {
        data.characterName = character.CharacterName;
        data.Job = character.Job;
        data.attributesData = character.GetComponent<Stats>().attributes;
        data.featuresData = character.GetComponent<Featureable>().SaveFeatures();

        data.skillsIndex.Clear();
        foreach (var skill in character.GetComponent<Skillable>().activeSkills)
            data.skillsIndex.Add(skill.Id);
    }

    public void CreateCharacter(string job)
    {
        //Delete current class
        if (this.character)
            GameObject.Destroy(this.character.gameObject);

        //create a character based on class
        var character = CharacterDatabase.Find(c => c.Job == job);
        if (!character)
            return;

        this.character = GameObject.Instantiate(character, characterPos.position, characterPos.rotation);

        //display text description
        ClassDescription.text = this.character.CharacterDescription;
        NameInput.text = "";
        //Notify others
        OnCharacterCreate.Invoke();
    }

    public void SetName()
    {
        character.CharacterName = NameInput.text;
    }

    public void RandomizeCharacter()
    {
        CreateNewData();
        CreateCharacter(CharacterDatabase[UnityEngine.Random.Range(0, CharacterDatabase.Count)].Job);
        character.CharacterName = NamesDatabase[UnityEngine.Random.Range(0, NamesDatabase.Count)];
        NameInput.text = character.CharacterName;
        SendMessage("Randomize");
    }

    public void SaveCharacter()
    {
        if(!character)
        {
            Debug.Log("No character chosen!");
            return;
        }
        if(character.CharacterName == "" || character.CharacterName == null)
        {
            Debug.Log("Name needed!");
            return;  
        }

        //Save player and broadcast event
        SaveData();
        OnSaveCharacter.Invoke(data);

        //delete player and data
        DeleteDisplayedCharacter();
        Panel.SetActive(false);
    }

    public void DeleteDisplayedCharacter()
    {
        if (character)
            Destroy(character.gameObject);

        //delete player and data
        data = null;
        character = null;

        //Update UI
        ClassDescription.text = "";
        NameInput.text = "";
    }

}
