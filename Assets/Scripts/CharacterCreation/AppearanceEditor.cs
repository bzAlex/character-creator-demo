﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AppearanceEditor : MonoBehaviour,IRandomizable
{
    CharacterCreator creator;

    List<Feature> characterFeatures;
    List<RectTransform> views = new List<RectTransform>();

    //UI
    public GameObject ItemButtonPrefab;
    public RectTransform featureViewParent;
    public GameObject FeatureViewPrefab;

    private void Start()
    {
        creator = GetComponent<CharacterCreator>();
        creator.OnCharacterCreate += RefreshView;
    }

    void RefreshView()
    {
        //clear views UI
        if(views.Count>0)
            foreach (var view in views)
                Destroy(view.gameObject);

        views.Clear();

        if (!creator.Character)
            return;
        characterFeatures = creator.Character.GetComponent<Featureable>().features;

        //Create a feature view for every part
        foreach (var feature in characterFeatures)
        {
            var parent = GameObject.Instantiate(FeatureViewPrefab, featureViewParent).GetComponent<RectTransform>();
            views.Add(parent);
            parent.Find("Feature Name").GetComponent<Text>().text = feature.Name;

            //fill every feature view with buttons
            for (int i = 0; i < feature.FeatureDatabase.Count; ++i)
            {
                var button = GameObject.Instantiate(ItemButtonPrefab, parent.Find("Scroll View/Viewport/Content")).GetComponent<Button>();

                if(feature.FeatureDatabase[i].Icon)
                    button.transform.Find("Image").GetComponent<Image>().sprite = feature.FeatureDatabase[i].Icon;

                //set button onclick (save int - Closure) 
                var temp = i;                   
                button.onClick.AddListener(delegate { feature.SetFeature(temp); });
            }
        }
    }

    public void Randomize()
    {
        foreach (var feature in characterFeatures)
            feature.SetFeature(UnityEngine.Random.Range(0, feature.FeatureDatabase.Count));
    }
}
