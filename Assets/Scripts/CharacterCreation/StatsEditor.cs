﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsEditor : MonoBehaviour,IRandomizable
{
    CharacterCreator creator;
    Stats characterStats;

    List<AttributeUI> AttributeEditor = new List<AttributeUI>();

    [SerializeField] GameObject AttributeUIPrefab;
    [SerializeField] RectTransform atrParent;

    private void Start()
    {
        creator = GetComponent<CharacterCreator>();
        creator.OnCharacterCreate += RefreshView;
    }

    public void RefreshView()
    {
        //delete values
        if (AttributeEditor.Count >0)
        foreach (var atrEditor in AttributeEditor)
            GameObject.Destroy(atrEditor.gameObject);

        AttributeEditor.Clear() ;

        if (!creator.Character)
            return;
        characterStats = creator.Character.GetComponent<Stats>();

        foreach (var atr in characterStats.attributes)
        {
            //create a button
            var atrButton = GameObject.Instantiate(AttributeUIPrefab, atrParent).GetComponent<AttributeUI>();
            AttributeEditor.Add(atrButton);
            atrButton.Atr = atr;
            atrButton.UpdateValue();
        }
    }

    public void Randomize()
    {
        foreach (var atr in characterStats.attributes)
            atr.Current = UnityEngine.Random.Range(0, atr.Max);
        //Update UI
        foreach (var atrEditor in AttributeEditor)
            atrEditor.UpdateValue();
    }
}
