﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class SkillsEditor : MonoBehaviour,IRandomizable
{
    CharacterCreator creator;
    Skillable skillable;


    [Header("UI")]
    public List<Button> SkillButtons;
    public Text SkillName, SkillDescription;

    private void Start()
    {
        creator = GetComponent<CharacterCreator>();
        creator.OnCharacterCreate += RefreshView;
    }

    public void RefreshView()
    {
        foreach (var button in SkillButtons)
        {
            button.interactable = true;
            button.GetComponent<Image>().sprite = null;
            button.onClick.RemoveAllListeners();
        }

        if (!creator.Character)
            return;

        skillable = creator.Character.GetComponent<Skillable>();

        //Update Buttons
        var size = skillable.skillDatabase.Count < SkillButtons.Count ?
                    skillable.skillDatabase.Count : SkillButtons.Count;

        for (int i =0;i< size;++i)
        {
            //Button set icon
            SkillButtons[i].GetComponent<Image>().sprite = skillable.skillDatabase[i].SkillIcon;

            //Check if skilldatabase already used
            if (skillable.activeSkills.Find(sk=>sk.Id == skillable.skillDatabase[i].Id))
                SkillButtons[i].interactable = false;

            //set button events
            var j = i;
            SkillButtons[i].onClick.AddListener(delegate { AddSkill(skillable.skillDatabase[j].Id,SkillButtons[j]); });
        }

    }

    public void RemoveSkills()
    {
        foreach (var b in SkillButtons)
            b.interactable = true;

        skillable.RemoveAllActiveSkills();

        SkillName.text = "";
        SkillDescription.text = "";
    }

    public void AddSkill(string id, Button button)
    {
        //If player has max active
        if (skillable.IsFull)
            return;

        skillable.AddSkill(id);

        //Disable button && update UI
        button.interactable = false;
        SkillName.text = skillable.GetFromDatabase(id).name;
        SkillDescription.text = skillable.GetFromDatabase(id).Description;
    }


    public void Randomize()
    {
        for(int i =0;i<skillable.MAXSKILLSIZE;++i)
        {
            //get a skill from database that is not in active skills
            var unusedSkills = new List<Skill>(skillable.skillDatabase.Where(sk => !skillable.activeSkills.Any(sk2 => sk2.Id == sk.Id)));
            //get random skill
            var skillID = unusedSkills[UnityEngine.Random.Range(0, unusedSkills.Count)].Id;
            skillable.AddSkill(skillID);
        }
        RefreshView();
    }
}
