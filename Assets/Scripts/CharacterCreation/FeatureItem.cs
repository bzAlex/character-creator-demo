﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatureItem : MonoBehaviour
{
    [SerializeField] Sprite icon;
    public Sprite Icon { get { return icon; } }

    //Add other info for item
}
