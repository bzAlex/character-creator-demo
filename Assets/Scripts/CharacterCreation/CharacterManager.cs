﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    public int SaveSlotsCount = 3;
    public CharacterCreator creator;

    [Header("UI")]
    public SaveSlotUI SaveSlotUIPrefab;
    public RectTransform SavePanel;
    List<SaveSlotUI> slotButtons = new List<SaveSlotUI>();


    string filePath;
    string fileName = "save";
    string fileExtension = ".json";
    int currentSlot = -1;           //Currently modified save slot


    private void Start()
    {
        filePath = Application.streamingAssetsPath + "/Saves/";
        JSONManager.CreateDirectory(filePath);
        creator.OnSaveCharacter += SaveCharacter;

        ConstructButtons();
        RefreshView();
    }

    void ConstructButtons()
    {
        for (int i = 0; i < SaveSlotsCount; ++i)
        {
            int j = i;
            var saveSlot = GameObject.Instantiate(SaveSlotUIPrefab, SavePanel).GetComponent<SaveSlotUI>();
            saveSlot.saveNumber = j;
            saveSlot.button.onClick.AddListener(delegate { OnSaveSlotPress(j); });

            slotButtons.Add(saveSlot);
        }
    }

    void RefreshView()
    {
        foreach (var slot in slotButtons)
        {
            //Check previous save data
            var characterData = JSONManager.LoadFile<CharacterData>(GetSavePath(slot.saveNumber));

            if (characterData != null)
                slot.UpdateText(characterData.characterName, characterData.Job, slot.saveNumber.ToString());
            else
                slot.UpdateText("Empty", "", slot.saveNumber.ToString());
        }
    }
    
    public void SaveCharacter(CharacterData data)
    {
        JSONManager.SaveFile<CharacterData>(GetSavePath(currentSlot), data);
        RefreshView();
    }

    public CharacterData LoadCharacter()
    {
        return JSONManager.LoadFile<CharacterData>(GetSavePath(currentSlot));
    }

    //For loading and creating
    void OnSaveSlotPress(int saveSlot)
    {
        currentSlot = saveSlot; //Update current slot number

        var characterData = LoadCharacter();

        if (characterData !=null)
            creator.LoadData(characterData);
        else
            creator.CreateNewData();
    }

    string GetSavePath(int saveSlot)
    {
        return filePath + fileName + saveSlot + fileExtension;
    }

}
