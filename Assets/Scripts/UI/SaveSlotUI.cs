﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveSlotUI: MonoBehaviour
{
    public int saveNumber;

    public Text CharacterName, Job, Number;
    public Button button;

    public void UpdateText(string characterName,string job, string number)
    {
        CharacterName.text = characterName;
        Job.text = job;
        Number.text = number;
    }
}