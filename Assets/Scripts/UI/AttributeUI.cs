﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AttributeUI : MonoBehaviour
{
    public Text AttributeName;
    public Text CurValue;
    Attribute atr;

    public Attribute Atr
    {
        get { return atr; }
        set
        {
            atr = value;
            AttributeName.text = atr.Name;          
        }
    }

    public void Increment()
    {
        atr.Current++;
    }

    public void Decrement()
    {
        atr.Current--;
    }

    public void UpdateValue()
    {
        CurValue.text = atr.Current.ToString();
    }
}