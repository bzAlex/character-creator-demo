﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Attribute
{
    public string Name;
    public int Max;
    public int IncrementValue;

    [SerializeField] int current;

    public int Current { get { return current; } set { current = (int)Mathf.Clamp(value, 0, Max); } }


    public Attribute(string name,int curr, int max=99, int incrementperLevel=1)
    {
        Name = name;
        Current = curr;
        Max = max;
        IncrementValue = incrementperLevel;
    }

}
