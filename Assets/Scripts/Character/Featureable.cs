﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Featureable : MonoBehaviour
{
    public List<Feature> features;

    private void OnEnable()
    {
        foreach (var feature in features)
        {
            feature.Equip();
        }
    }

    private void OnDisable()
    {
        foreach (var feature in features)
        {
            feature.Unequip();
        }
    }

    public void LoadFeatures(List<CharacterData.featureData> featuresIndex)
    {
        foreach (var fI in featuresIndex)
        {
            //Find feature name and set current feature
            var feature = features.Find(f => f.Name == fI.key);

            if(feature !=null)
                feature.SetFeature(fI.value);
        }
    }

    public List<CharacterData.featureData> SaveFeatures()
    {
        var d = new List<CharacterData.featureData>();
        foreach (var feature in features)
            d.Add(new CharacterData.featureData(feature.Name,feature.curIndex));
        
        return d;
    }
}


