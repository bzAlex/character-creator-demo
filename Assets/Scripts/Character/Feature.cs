﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Feature
{
    public string Name;
    public Transform Parent;
    public List<FeatureItem> FeatureDatabase;    //Available features
    GameObject currentFeature;                  //currently worn feature

    [HideInInspector]
    public int curIndex;

    public void SetFeature(int index)
    {
        //Replace current equipped item
        if (currentFeature)
            GameObject.Destroy(currentFeature.gameObject);


        if (index >= FeatureDatabase.Count)
            Debug.Log("Index "+index+" higher than choice count.");

        //Instantiate copy of item
        currentFeature = GameObject.Instantiate(FeatureDatabase[index].gameObject, Parent);
        curIndex = index;
        Equip();
    }

    public void Equip()
    {
        if(currentFeature)
            currentFeature.gameObject.SetActive(true);
    }

    public void Unequip()
    {
        if(currentFeature)
            currentFeature.gameObject.SetActive(false);
    }

}