﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterData
{
    [System.Serializable]
    public struct featureData
    {
        public string key;
        public int value;

        public featureData(string key, int value)
        {
            this.key = key;
            this.value = value;
        }
    }

    public string characterName;
    public string Job;

    public List<string> skillsIndex = new List<string>();
    public List<featureData> featuresData = new List<featureData>(); //Feature Name, Current Index
    public List<Attribute> attributesData = new List<Attribute>();
}
