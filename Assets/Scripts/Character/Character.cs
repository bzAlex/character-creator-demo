﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Character : MonoBehaviour
{
    [SerializeField] string characterName;

    public string Job;
    [TextArea(5, 10)] public string CharacterDescription;

    public string CharacterName { get { return characterName; } set { characterName = value; } }

}
