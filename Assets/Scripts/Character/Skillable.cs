﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Can cast and acquire skills.
/// </summary>
public class Skillable : MonoBehaviour
{
    public readonly int MAXSKILLSIZE = 2;

    public List<Skill> activeSkills;
    public List<Skill> skillDatabase;   //Unused

    public Transform SkillParent;

    public bool IsFull { get { return activeSkills.Count >= MAXSKILLSIZE; } }

    public void UseSkill(Skill skill)
    {
        skill.Cast();
    }

    public void AddSkill(string id)
    {
        if (IsFull)
        {
            Debug.Log("Full capacity!");
            return;
        }


        var skill = GetFromDatabase(id);
        if (!skill)
        {
            Debug.Log("No skill found!");
            return;
        }
           
        var newSkill = GameObject.Instantiate(skill, SkillParent).GetComponent<Skill>();
        newSkill.caster = this;
        activeSkills.Add(newSkill);
    }

    public Skill GetFromDatabase(string id)
    {
        if (skillDatabase.Count <= 0)
        {
            Debug.Log("No unused skills.");
            return null;
        }

        //Fetch item from database using ID
        var skill = skillDatabase.Where(s => s.Id == id).FirstOrDefault();

        if (!skill)
            Debug.Log("No skill found.");

        return skill;
    }

    public void LoadSkills(List<string> ids)
    {
        foreach (var id in ids)
        {
            AddSkill(id);
        }
    }

    public void RemoveAllActiveSkills()
    {
        //delete instances
        foreach (var skill in activeSkills)
            GameObject.Destroy(skill.gameObject);

        activeSkills.Clear();
    }

}
