﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public float Duration;
    public float AOE;
    public float DamagePerMeteor;

    Skill skill;
    CooldownTimer cooldown;

    private void Start()
    {
        skill = GetComponent<Skill>();
        cooldown = GetComponent<CooldownTimer>();
        skill.OnSkillEnter += Execute;

        cooldown = GetComponent<CooldownTimer>();
    }

    void Execute()
    {
        //Check for cooldown script
        if (cooldown)
            if (!cooldown.isReady)
                return;

        //Check for other relevant components to this skill
        // ex. (target type, requirements, damage multiplier)


        //check cooldown
        if (cooldown)
            cooldown.Reset();

    }

}
