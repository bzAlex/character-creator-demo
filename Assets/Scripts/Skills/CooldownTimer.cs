﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// For skills with cooldown.
/// </summary>
public class CooldownTimer : MonoBehaviour
{
    public float CooldownTime;

    float curDuration;
    float timer;

    public bool isReady { get { return timer >= curDuration; } }

    public void Reset()
    {
        curDuration = CooldownTime;
    }

    private void Update()
    {
        if (timer < curDuration)
            timer += Time.deltaTime;
        else
            timer = curDuration;
    }
}
