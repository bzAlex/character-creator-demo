﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Skill : MonoBehaviour
{
    public string Id;
    public string Name;
    public string Description;
 
    public Sprite SkillIcon;

    public Skillable caster;


    public event Action OnSkillEnter;
    public event Action OnSkillChain;
    public event Action OnSkillExit;    //When player cancel execution(ex. channeled)

    public void Cast()
    {
        OnSkillEnter.Invoke();
    }

}
